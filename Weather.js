// Weather.js
var tools = require('altrac-conversions');

var statusWeather = function statusWeather(
  status,
  device
) {
  const reading0 = device.reading0 || {};
  // const reading1 = device.reading1 || {};

  const inputPower = reading0['186'] & 0x4;
  if (inputPower) {
    status.other.push({
      id: 'weatherDevicePowerOn',
      rank: 100,
      code: 'normal',
      category: 'electricity',
      shortMessage: 'Weather Device has power',
      message: 'Weather Device ' + device.physical.deviceNumber + ' has power.',
    });
  } else {
    status.other.push({
      id: 'weatherDevicePowerOff',
      rank: 15,
      code: 'disconnected',
      category: 'controller',
      shortMessage: 'Weather Device does not have power',
      message: 'Weather Device ' + device.physical.deviceNumber + ' does not have power.',
    });
    status.deviceHeadlineDanger.enabled = true;
    status.deviceHeadlineDanger.text.push('Altrac is not receiving power');
    return false;
  }
};

module.exports = statusWeather;
