// Pump.js
var tools = require('altrac-conversions');
var temperatureState = require('./Temperature').temperatureState;

/*

  What we want out of this:
    - All the calculated statuses of the machines
    - Encoded in a ranked array of items

  Strategy: 
    - Determine whether device thinks engine should be on
    - Determine current engine state
      - For non-modbus controlled engines, do this based on RPM and time since
        machine started
      - For modbus controlled engines, do this based on feedback from controller
    - Determine what the RPM is of the machine and whether reasonable
    - Determine if machine's temperature reading is valid
    - Use temperature reading to determine if engine should be on


*/

const statusPump = function StatusPump(
  status,
  device
) {

  /*
  
    Determine if machine should be running based on tempeature

  */

  const deviceName = device.physical.deviceNumber;
  const reading0 = device.reading0 || {};
  const reading1 = device.reading1 || {};
  const machineOnOff = ((reading0['131'] & 1) >>> 0);
  var machineRunning = machineState(reading0['134']);
  const powerStatus = powerState(status, reading0, device.physical);

  /*


    Controller and engine status


  */

  var controller = 'notIntegrated';
  if (
    device.interface_versioned &&
    device.interface_versioned.data &&
    device.interface_versioned.data.other &&
    device.interface_versioned.data.other.controller
  ) {
    controller = device.interface_versioned.data.other.controller;
  } else if (
    device.physical &&
    device.physical.controller
  ) {
    controller = device.physical.controller;
  }
  var noController = false;

  switch (controller) {
    case 'inverter':
      controllerInverter(status, reading0, machineOnOff, machineRunning, deviceName, device.physical);
      break;
    default:
      noController = true;
  }

  controllerNotIntegrated(status, machineOnOff, machineRunning, deviceName, noController);


  /*


    Other pump sensors


  */
  if (
    device.interface_versioned &&
    device.interface_versioned.data &&
    device.interface_versioned.data.other &&
    device.interface_versioned.data.other.inThree
  ) {
    switch (device.interface_versioned.data.other.inThree) {
      case 'pressure':
        pressureState(status, reading0, machineOnOff, machineRunning, deviceName, device.physical);
        break;
      default:
        break;
    }
  }
};

module.exports = statusPump;

const machineState = function machineState(runSignal, offRpm, highRpm) {
  if (offRpm && highRpm) {
    var runSignalTextState = tools.runSignalToState(runSignal, offRpm, highRpm);
    return (runSignalTextState.toUpperCase() === 'LOW' || runSignalTextState.toUpperCase() === 'HIGH') ? true : false;
  } else {
    return runSignal > 0 ? true : false;
  }
}

const controllerNotIntegrated = function controllerNotIntegrated(status, machineOnOff, machineRunning, deviceName, important) {
  if (machineOnOff && machineRunning) {
    status.other.push({
      id: 'pumpRunningState',
      rank: important ? 10 : 100,
      code: 'success',
      category: 'engine',
      shortMessage: 'Machine Running',
      message: 'Pump ' + deviceName + ' is running',
    });
  } else if (!machineOnOff && machineRunning) {
    status.other.push({
      id: 'pumpShouldBeOffState',
      rank: important ? 30 : 100,
      code: 'warning',
      category: 'engine',
      shortMessage: 'Machine Running',
      message: 'Pump ' + deviceName + ' should be off, but is running',
    });
  } else if (machineOnOff && !machineRunning) {
    status.other.push({
      id: 'pumpShouldBeOnState',
      rank: important ? 10 : 100,
      code: 'danger',
      category: 'engine',
      shortMessage: 'Not Running',
      message: 'Pump ' + deviceName + ' should be running',
    });
  } else {
    status.other.push({
      id: 'pumpStoppedState',
      rank: important ? 100 : 100,
      code: 'normal',
      category: 'engine',
      shortMessage: 'Engine Off',
      message: 'Engine at pump ' + deviceName + ' is off',
    });
  }
}

const controllerInverter = function controller(status, reading0, machineOnOff, machineRunning, deviceName, physical) {
  const engineState = reading0[140];
  const controllerAutoMode = reading0[202];
  const controllerAutoSwitch = tools.percentToDigital(reading0[143] / 10000);
  const controllerRunMode = reading0[203];
  const controllerPower = tools.percentToDigital(reading0[186] & 0x52);

  if (controllerAutoMode === 1) {
    status.other.push({
      id: 'pumpControllerAutoOn',
      rank: 100,
      code: 'normal',
      category: 'controller',
      shortMessage: 'A/S Auto On',
      message: 'Pump ' + deviceName + ' controller is in auto mode',
    });
    status.stateIndicator = true;
  } else {
    status.other.push({
      id: 'pumpControllerAutoOff',
      rank: 50,
      code: 'normal',
      category: 'controller',
      shortMessage: 'A/S Auto Off',
      message: 'Pump ' + deviceName + ' controller is not in auto mode',
    });
    status.stateIndicator = false;
  }

  if (
    !controllerAutoMode
    && controllerRunMode
  ) {
    if (controllerAutoSwitch) {
      status.other.push({
        id: 'pumpControllerManualRun',
        rank: 100,
        code: 'normal',
        category: 'controller',
        shortMessage: 'A/S Auto On',
        message: 'Pump ' + deviceName + ' controller is in manual and switch position relay has power',
      });
    } else {
      status.other.push({
        id: 'pumpControllerManualRunSwitchOff',
        rank: 20,
        code: 'disconnected',
        category: 'controller',
        shortMessage: 'A/S switch off',
        message: 'Pump ' + deviceName + ' controller is in manual should be running, but switch is in wrong position',
      });
      status.stateIndicator = false;
      status.deviceHeadlineDanger.enabled = true;
      status.deviceHeadlineDanger.text.push('Switch on equipment is not in auto position');
    }
  } else {
    if (!controllerAutoSwitch) {
      status.other.push({
        id: 'pumpControllerManualRunSwitchOff',
        rank: 50,
        code: 'normal',
        category: 'controller',
        shortMessage: 'A/S switch off',
        message: 'Pump ' + deviceName + ' controller is in manual switch is off',
      });
      status.stateIndicator = false;
      status.deviceHeadlineWarning.enabled = true;
      status.deviceHeadlineWarning.text.push('Switch on equipment is not in auto position');
    }
  }

  var controllerId = 'pump';
  var controllerRank = 10;
  var controllerCode = 'normal';
  var controllerShortMessage = 'Unknown issue';
  var controllerMessage = 'Unknown issue';

  switch (engineState) {
    case 1:
      controllerId = 'pumpControllerEngineStopped';
      controllerRank = 100;
      controllerCode = 'normal';
      controllerShortMessage = 'Engine Stopped';
      controllerMessage = 'Pump ' + deviceName + ' is stopped';
      break;
    case 6:
      controllerId = 'pumpControllerCrankOn';
      controllerRank = 10;
      controllerCode = 'info';
      controllerShortMessage = 'Crank on';
      controllerMessage = 'Pump ' + deviceName + ' crank on';
      break;
    case 8:
      controllerId = 'pumpControllerFalseStart';
      controllerRank = 10;
      controllerCode = 'danger';
      controllerShortMessage = 'False Start';
      controllerMessage = 'Pump ' + deviceName + ' false start';
      break;
    case 12:
      controllerId = 'pumpControllerRunning';
      controllerRank = 10;
      controllerCode = 'success';
      controllerShortMessage = 'Running';
      controllerMessage = 'Pump ' + deviceName + ' running loaded';
      break;
    default:
      controllerId = 'pumpControllerUnknownEngineState';
      controllerRank = 30;
      controllerCode = 'danger';
      controllerShortMessage = 'Unknown engine state';
      controllerMessage = 'Pump ' + deviceName + ' unexpected engine state';
      break;
  }

  status.other.push({
    id: controllerId,
    rank: controllerRank,
    code: controllerCode,
    category: 'controller',
    shortMessage: controllerShortMessage,
    message: controllerMessage,
  });

  if (engineState >= 8 && engineState <= 13 && !machineRunning) {
    status.deviceHeadlineDanger.enabled = true;
    status.deviceHeadlineDanger.text.unshift('Machine should be ON');
    status.other.push({
      id: 'pumpShouldBeOnState',
      rank: 1,
      code: 'danger',
      category: 'engine',
      shortMessage: 'Controller Issue',
      message: 'Run signal is off, pump ' + deviceName + ' should be running ',
    });
  }
};

const pressureState = function pressureState(status, reading0, machineOnOff, machineRunning, deviceName, physical) {
  const controllerAutoMode = reading0[202];
  /*


    Pressure sensor status


  */

  const pressure = tools.fourToTwenty(
    reading0['133'],
    physical.min || 0,
    physical.max || 100,
    physical.zero || 0,
    physical.precision || 0
  );
  if (pressure === 'ERH' || pressure === 'OFF') {
    if (
      !controllerAutoMode
      && (machineRunning || machineOnOff)
    ) {
      status.other.push({
        id: 'pumpControllerAutoPressureBadReading',
        rank: 5,
        code: status.other.slice(-1)[0].code,
        category: 'sensors',
        shortMessage: 'Pump running pressure sensor bad',
        message: 'Pump ' + deviceName + ' is running but pressure sensor is bad',
      });
    }
    status.other.push({
      id: 'pressureBadReading',
      rank: 20,
      code: 'deviceProblem',
      category: 'sensors',
      shortMessage: 'Pressure sensor reading bad',
      message: 'Pump ' + deviceName + ' pressure sensor reading is bad',
    });
    status.deviceHeadlineDanger.enabled = true;
    status.deviceHeadlineDanger.text.push('Pressure sensor fault');
  }
}

const powerState = function powerState(status, reading0, physical) {
  const inputPower = reading0['186'] & 0x4;
  /*

      //System Status Register
      //NOTE: This is a read-only register
      REG08
      BIT
      --- VBUS status
      7: VBUS_STAT[1] | 00: Unknown (no input, or DPDM detection incomplete), 01: USB host
      6: VBUS_STAT[0] | 10: Adapter port, 11: OTG
      --- Charging status
      5: CHRG_STAT[1] | 00: Not Charging,  01: Pre-charge (<VBATLOWV)
      4: CHRG_STAT[0] | 10: Fast Charging, 11: Charge termination done
      3: DPM_STAT   0: Not DPM
              1: VINDPM or IINDPM
      2: PG_STAT    0: Power NO Good :(
              1: Power Good :)
      1: THERM_STAT 0: Normal
              1: In Thermal Regulation (HOT)
      0: VSYS_STAT  0: Not in VSYSMIN regulation (BAT > VSYSMIN)
              1: In VSYSMIN regulation (BAT < VSYSMIN)ault is 3.5V (101)
      0: Reserved

  */
  if (inputPower) {
    status.other.push({
      id: 'pumpControllerPowerOn',
      rank: 100,
      code: 'normal',
      category: 'electricity',
      shortMessage: 'Controller has power',
      message: 'Controller has power at ' + physical.deviceNumber + ' ',
    });
  } else {
    status.other.push({
      id: 'pumpControllerPowerOff',
      rank: 15,
      code: 'disconnected',
      category: 'controller',
      shortMessage: 'Controller does not have power',
      message: 'Controller does not have power at ' + physical.deviceNumber + ' ',
    });
    status.deviceHeadlineDanger.enabled = true;
    status.deviceHeadlineDanger.text.push('Altrac is not receiving power');
    return false;
  }
  return true;
};
