// WindMachine.js
var tools = require('altrac-conversions');
var temperatureState = require('./Temperature').temperatureState;

/*

  What we want out of this:
    - All the calculated statuses of the machines
    - Encoded in a ranked array of items

  Strategy: 
    - Determine whether device thinks engine should be on
    - Determine current engine state
      - For non-modbus controlled engines, do this based on RPM and time since
        machine started
      - For modbus controlled engines, do this based on feedback from controller
    - Determine what the RPM is of the machine and whether reasonable
    - Determine if machine's temperature reading is valid
    - Use temperature reading to determine if engine should be on


*/

const statusWindMachine = function StatusWindMachine(
  status,
  device
) {

  /*
  
    Determine if machine should be running based on tempeature

  */

  const reading0 = device.reading0 || {};
  const reading1 = device.reading1 || {};
  const tempState = temperatureState(reading0, device.application_settings);
  const tempState1 = temperatureState(reading1, device.application_settings);
  const engineOnOff = ((reading0['131'] & 1) >>> 0);
  var engineRPMUp = false;
  if (
    device.application_settings &&
    device.application_settings.settings &&
    device.application_settings.settings.calibrate1 &&
    device.application_settings.settings.calibrate2
  ) {
    engineRPMUp = rpmState(reading0['134'], device.application_settings.settings.calibrate1, device.application_settings.settings.calibrate2);
  } else if (device.physical.offRpm && device.physical.highRpm) {
    engineRPMUp = rpmState(reading0['134'], device.physical.offRpm, device.physical.highRpm);
  } else {
    engineRPMUp = rpmState(reading0['134']);
  }
  const powerStatus = powerState(status, reading0, device.physical);

  switch(tempState) {
    case 'temperatureOldReading':
    case 'temperatureBadReading':
      status.other.push({
        id: 'temperatureBadReading',
        rank: 20,
        code: 'deviceProblem',
        category: 'temperature',
        shortMessage: 'Temperature Sensor Error',
        message: 'Wind Machine ' + device.physical.deviceNumber + ' has a bad temperature reading',
      });
      status.deviceHeadlineDanger.enabled = true;
      status.deviceHeadlineDanger.text.push('Temperature probe fault');
      break;
    case 'temperatureBadSetPoints':
      status.other.push({
        id: 'windMachineBadSetPoints',
        rank: 20,
        code: 'deviceProblem',
        category: 'temperature',
        shortMessage: 'Bad set points',
        message: 'Wind Machine ' + device.physical.deviceNumber + ' has bad start and stop points',
      });
      break;
    case 'temperatureBelowStart':
      status.deviceHeadlineDanger.enabled = true;
      status.deviceHeadlineDanger.text.push('Ambient temperature below start temperature');
      if (!engineOnOff || !engineRPMUp) {
        // this means the engine is not running and its too cold!
        if (tempState1 === 'temperatureBelowStart') {
          status.other.push({
            id: 'windMachineBelowStartNotRunningConfirmed',
            rank: 14,
            code: 'danger',
            category: 'protection',
            shortMessage: 'Below start not running',
            message: 'Wind Machine ' + device.physical.deviceNumber + ' is below start point, but not running.',
          });
        } else {
          status.other.push({
            id: 'windMachineBelowStartNotRunning',
            rank: 100,
            code: 'normal',
            category: 'protection',
            shortMessage: 'Below start not running',
            message: 'Wind Machine ' + device.physical.deviceNumber + ' is below start point, but not running.',
          });
        }
      } else {
        status.other.push({
          id: 'windMachineBelowStartRunning',
          rank: 20,
          code: 'normal',
          category: 'protection',
          shortMessage: 'Below start running',
          message: 'Wind Machine ' + device.physical.deviceNumber + ' is below start point, running.',
        });
      }
      break;
    case 'temperatureBetweenStartAndStop':
      if (!engineOnOff || !engineRPMUp) {
        // this means the engine is not running and its too cold!
        status.other.push({
          id: 'windMachineBetweenSetPointsNotRunning',
          rank: 100,
          code: 'normal',
          category: 'protection',
          shortMessage: 'Between set points not running',
          message: 'Wind Machine ' + device.physical.deviceNumber + ' is between set points and not running',
        });
      } else {
        status.other.push({
          id: 'windMachineBetweenSetPointsRunning',
          rank: 20,
          code: 'normal',
          category: 'protection',
          shortMessage: 'Between set points running',
          message: 'Wind Machine ' + device.physical.deviceNumber + ' is between set points and running.',
        });
      }
      break;
    case 'temperatureAboveStop':
      if (!engineOnOff || !engineRPMUp) {
        // this means the engine is not running and its too cold!
        status.other.push({
          id: 'windMachineAboveStopNotRunning',
          rank: 100,
          code: 'normal',
          category: 'protection',
          shortMessage: 'Above stop point not running',
          message: 'Wind Machine ' + device.physical.deviceNumber + ' is above stop point and not running',
        });
      } else {
        status.other.push({
          id: 'windMachineAboveStopRunning',
          rank: 50,
          code: 'normal',
          category: 'protection',
          shortMessage: 'Above stop point running',
          message: 'Wind Machine ' + device.physical.deviceNumber + ' is above stop point and running.',
        });
        status.warningIndicator = true;
        status.deviceHeadlineWarning.enabled = true;
        status.deviceHeadlineWarning.text.push('Ambient temperature above stop temperature');
      }
      break;
    default:

  }


  /*


    Controller and engine status


  */

  var controller = 'notIntegrated';
  if (
    device.interface_versioned &&
    device.interface_versioned.data &&
    device.interface_versioned.data.other &&
    device.interface_versioned.data.other.controller
  ) {
    controller = device.interface_versioned.data.other.controller;
  } else if (
    device.physical &&
    device.physical.controller
  ) {
    controller = device.physical.controller;
  }
  const controllerState = ((reading0[144] & 0xFF) >>> 0) - 10;
  const setPointState = reading0[144] >>> 8;
  var noController = false;

  switch (controller) {
    case 'pv101':
    case 'callToStart':
      status.other.push({
        id: 'windMachineControllerConnected',
        rank: 100,
        code: 'normal',
        category: 'controller',
        shortMessage: 'A/S Connected',
        message: 'Wind Machine ' + device.physical.deviceNumber + ' controller is connected',
      });
      controllerPv101(status, reading0, engineRPMUp, device.physical);
      break;
    case 'mpc20':
      if (controllerState > 0) {
        status.other.push({
          id: 'windMachineControllerConnected',
          rank: 100,
          code: 'normal',
          category: 'controller',
          shortMessage: 'A/S Connected',
          message: 'Wind Machine ' + device.physical.deviceNumber + ' controller is connected',
        });
        controllerMpc20(status, reading0, engineRPMUp, device.physical.deviceNumber, tempState);
      } else {
        status.other.push({
          id: 'windMachineControllerDisconnected',
          rank: 15,
          code: 'disconnected',
          category: 'controller',
          shortMessage: 'A/S Disconnected',
          message: 'Wind Machine ' + device.physical.deviceNumber + ' controller is not connected',
        });
        noController = true;
      }
      break;
    case 'electric':
      status.other.push({
        id: 'windMachineControllerNotIntegrated',
        rank: 100,
        code: 'disconnected',
        category: 'controller',
        shortMessage: 'No A/S Integration',
        message: 'Wind Machine ' + device.physical.deviceNumber + ' does not have an A/S controller Altrac integrates with',
      });
      controllerElectric(status, reading0, engineRPMUp, device.physical, tempState);
      break;
    default: 
      status.other.push({
        id: 'windMachineControllerNotIntegrated',
        rank: 100,
        code: 'disconnected',
        category: 'controller',
        shortMessage: 'No A/S Integration',
        message: 'Wind Machine ' + device.physical.deviceNumber + ' does not have an A/S controller Altrac integrates with',
      });
      noController = true;
  }

  controllerNotIntegrated(status, engineOnOff, engineRPMUp, device.physical.deviceNumber, noController);

  /*


    Other wind machine sensors


  */

  fuelLevelState(status, reading0, device.physical);  
};

module.exports = statusWindMachine;

const rpmState = function rpmState(rpm, offRpm, highRpm) {
  if (offRpm && highRpm) {
    var rpmTextState = tools.rpmToState(rpm, offRpm, highRpm);
    return (rpmTextState.toUpperCase() === 'LOW' || rpmTextState.toUpperCase() === 'HIGH') ? true : false;
  } else {
    return rpm > 500 ? true : false;
  }
}

const controllerNotIntegrated = function controllerNotIntegrated(status, engineOnOff, engineRPMUp, deviceNumber, important) {
  if (engineOnOff && engineRPMUp) {
    status.other.push({
      id: 'windMachineRunningState',
      rank: important ? 10 : 100,
      code: 'success',
      category: 'engine',
      shortMessage: 'Engine Running',
      message: 'Wind Machine ' + deviceNumber + ' is running',
    });
  } else if (!engineOnOff && engineRPMUp) {
    status.other.push({
      id: 'windMachineShouldBeOffState',
      rank: important ? 30 : 100,
      code: 'warning',
      category: 'engine',
      shortMessage: 'Engine Running',
      message: 'Wind Machine ' + deviceNumber + ' should be off, but is running',
    });
  } else if (engineOnOff && !engineRPMUp) {
    status.other.push({
      id: 'windMachineShouldBeOnState',
      rank: important ? 10 : 100,
      code: 'danger',
      category: 'engine',
      shortMessage: 'Not Running',
      message: 'Wind Machine ' + deviceNumber + ' should be running',
    });
  } else {
    status.other.push({
      id: 'windMachineStoppedState',
      rank: important ? 100 : 100,
      code: 'normal',
      category: 'engine',
      shortMessage: 'Engine Off',
      message: 'Engine at wind machine ' + deviceNumber + ' is off',
    });
  }
}

const controllerMpc20 = function controllerMpc20(status, reading0, engineRPMUp, deviceNumber, tempState) {
  const engineState = reading0[140];
  const controllerAutoMode = reading0[202];

  if (controllerAutoMode === 1) {
    status.other.push({
      id: 'windMachineControllerAutoOn',
      rank: 100,
      code: 'normal',
      category: 'controller',
      shortMessage: 'A/S Auto On',
      message: 'Wind Machine ' + deviceNumber + ' controller is in auto mode',
    });
    status.stateIndicator = true;
  } else {
    status.other.push({
      id: 'windMachineControllerAutoOff',
      rank: 20,
      code: 'normal',
      category: 'controller',
      shortMessage: 'A/S Auto Off',
      message: 'Wind Machine ' + deviceNumber + ' controller is not in auto mode',
    });
    status.stateIndicator = false;
  }

  var controllerId = 'windMachine';
  var controllerRank = 10;
  var controllerCode = 'normal';
  var controllerShortMessage = 'Unknown issue';
  var controllerMessage = 'Unknown issue';

  switch (engineState) {
    case 0:
      controllerId = 'windMachineControllerECUDelay';
      controllerRank = 100;
      controllerCode = 'normal';
      controllerShortMessage = 'ECU Delay';
      controllerMessage = 'Wind Machine ' + deviceNumber + ' is in ECU delay';
      break;
    case 1:
      controllerId = 'windMachineControllerEngineStopped';
      controllerRank = 100;
      controllerCode = 'normal';
      controllerShortMessage = 'Engine Stopped';
      controllerMessage = 'Wind Machine ' + deviceNumber + ' is stopped';
      break;
    case 2:
      controllerId = 'windMachineControllerStandby';
      controllerRank = 100;
      controllerCode = 'normal';
      controllerShortMessage = 'Controller Standby';
      controllerMessage = 'Wind Machine ' + deviceNumber + ' MPC-20 controller in standby';
      break;
    case 3:
      controllerId = 'windMachineControllerPrestart1';
      controllerRank = 10;
      controllerCode = 'info';
      controllerShortMessage = 'Prestart 1';
      controllerMessage = 'Wind Machine ' + deviceNumber + ' in prestart delay 1';
      break;
    case 4:
      controllerId = 'windMachineControllerChecksafe';
      controllerRank = 10;
      controllerCode = 'info';
      controllerShortMessage = 'Checksafe';
      controllerMessage = 'Wind Machine ' + deviceNumber + ' in checksafe';
      break;
    case 5:
      controllerId = 'windMachineControllerPrestart2';
      controllerRank = 10;
      controllerCode = 'info';
      controllerShortMessage = 'Prestart 2';
      controllerMessage = 'Wind Machine ' + deviceNumber + ' in prestart delay 2';
      break;
    case 6:
      controllerId = 'windMachineControllerCrankOn';
      controllerRank = 10;
      controllerCode = 'info';
      controllerShortMessage = 'Crank on';
      controllerMessage = 'Wind Machine ' + deviceNumber + ' crank on';
      break;
    case 7:
      controllerId = 'windMachineControllerCrankReset';
      controllerRank = 10;
      controllerCode = 'info';
      controllerShortMessage = 'Crank Rest';
      controllerMessage = 'Wind Machine ' + deviceNumber + ' crank rest';
      break;
    case 8:
      controllerId = 'windMachineControllerFalseStart';
      controllerRank = 1;
      controllerCode = 'danger';
      controllerShortMessage = 'False Start';
      controllerMessage = 'Wind Machine ' + deviceNumber + ' false start';
      break;
    case 9:
      controllerId = 'windMachineControllerWarmup';
      controllerRank = 10;
      controllerCode = 'info';
      controllerShortMessage = 'Warmup';
      controllerMessage = 'Wind Machine ' + deviceNumber + ' in warmup delay';
      break;
    case 10:
      controllerId = 'windMachineControllerLineFill1';
      controllerRank = 10;
      controllerCode = 'success';
      controllerShortMessage = 'Line Fill 1';
      controllerMessage = 'Wind Machine ' + deviceNumber + ' in line fill 1';
      break;
    case 11:
      controllerId = 'windMachineControllerLineFill2';
      controllerRank = 10;
      controllerCode = 'success';
      controllerShortMessage = 'Line Fill 2';
      controllerMessage = 'Wind Machine ' + deviceNumber + ' in line fill 2';
      break;
    case 12:
      controllerId = 'windMachineControllerRunning';
      controllerRank = 10;
      controllerCode = 'success';
      controllerShortMessage = 'Running';
      controllerMessage = 'Wind Machine ' + deviceNumber + ' running loaded';
      break;
    case 13:
      controllerId = 'windMachineControllerCooldown';
      controllerRank = 10;
      controllerCode = 'info';
      controllerShortMessage = 'Cooldown';
      controllerMessage = 'Wind Machine ' + deviceNumber + ' in cooldown delay';
      break;
    case 14:
      controllerId = 'windMachineControllerStopping';
      controllerRank = 10;
      controllerCode = 'info';
      controllerShortMessage = 'Stopping';
      controllerMessage = 'Wind Machine ' + deviceNumber + ' energize to stop';
      break;
    case 15:
      controllerId = 'windMachineControllerSpindown';
      controllerRank = 10;
      controllerCode = 'info';
      controllerShortMessage = 'Spindown';
      controllerMessage = 'Wind Machine ' + deviceNumber + ' in spindown delay';
      break;
    case 16:
      controllerId = 'windMachineControllerWaitToStart';
      controllerRank = 10;
      controllerCode = 'info';
      controllerShortMessage = 'Wait To Start';
      controllerMessage = 'Wind Machine ' + deviceNumber + ' in wait to start delay';
      break;
    default:
      controllerId = 'windMachineControllerMPC20Issue';
      controllerRank = 30;
      controllerCode = 'danger';
      controllerShortMessage = 'MPC-20 Issue';
      controllerMessage = 'Wind Machine ' + deviceNumber + ' unexpected engine state';
      break;
  }

  status.other.push({
    id: controllerId,
    rank: controllerRank,
    code: controllerCode,
    category: 'controller',
    shortMessage: controllerShortMessage,
    message: controllerMessage,
  });

  if (engineState >= 8 && engineState <= 13 && !engineRPMUp) {
    status.other.push({
      id: 'windMachineControllerIssue',
      rank: 1,
      code: 'danger',
      category: 'engine',
      shortMessage: 'Controller Issue',
      message: 'RPM is too low for controller state at wind machine ' + deviceNumber + ' ',
    });
  }

  /*

    In the scenario that the wind machine is running and the temperature probe is bad

  */
  if (tempState === 'temperatureOldReading' || tempState === 'temperatureBadReading') {
    if (controllerAutoMode > 0) {
      status.other.push({
        id: 'windMachineControllerAutoBadTempProbe',
        rank: 2,
        code: 'danger',
        category: 'controller',
        shortMessage: 'Temperature Sensor Error',
        message: 'Machine has a bad temp sensor and is in auto mode ' + deviceNumber + ' ',
      });
    }
  }

  /*
  
    Deal with warning and shutdown status

  */

  // first shutdown
  if (reading0['141']) {
    status.deviceHeadlineDanger.enabled = true;
    if (reading0['141'] & Math.pow(2, 0)) { status.deviceHeadlineDanger.text.push('Overspeed SD'); }
    if (reading0['141'] & Math.pow(2, 1)) { status.deviceHeadlineDanger.text.push('Underspeed SD'); }
    if (reading0['141'] & Math.pow(2, 2)) { status.deviceHeadlineDanger.text.push('Overcrank SD'); }
    if (reading0['141'] & Math.pow(2, 3)) { status.deviceHeadlineDanger.text.push('Low Oil Pressure SD'); }
    if (reading0['141'] & Math.pow(2, 4)) { status.deviceHeadlineDanger.text.push('High Engine Temp SD'); }
    if (reading0['141'] & Math.pow(2, 5)) { status.deviceHeadlineDanger.text.push('Low Fuel SD'); }
    if (reading0['141'] & Math.pow(2, 6)) { status.deviceHeadlineDanger.text.push('Low Discharge Pressure SD'); }
    if (reading0['141'] & Math.pow(2, 7)) { status.deviceHeadlineDanger.text.push('High Discharge Pressure SD'); }
    if (reading0['141'] & Math.pow(2, 8)) { status.deviceHeadlineDanger.text.push('Speed Signal Lost SD'); }
    if (reading0['141'] & Math.pow(2, 9)) { status.deviceHeadlineDanger.text.push('Low Lube Level SD'); }
    if (reading0['141'] & Math.pow(2, 10)) { status.deviceHeadlineDanger.text.push('Fuel Leak SD'); }
    if (reading0['141'] & Math.pow(2, 11)) { status.deviceHeadlineDanger.text.push('Fuel Filter Restriction SD'); }
    if (reading0['141'] & Math.pow(2, 12)) { status.deviceHeadlineDanger.text.push('Air Damper Closed SD no'); }
    if (reading0['141'] & Math.pow(2, 13)) { status.deviceHeadlineDanger.text.push('Air Filter Restriction SD'); }
    if (reading0['141'] & Math.pow(2, 14)) { status.deviceHeadlineDanger.text.push('Oil Filter Restriction SD'); }
    if (reading0['141'] & Math.pow(2, 15)) { status.deviceHeadlineDanger.text.push('Remote Stop SD'); }
    if (reading0['141'] & Math.pow(2, 16)) { status.deviceHeadlineDanger.text.push('Coolant Level SD'); }
    if (reading0['141'] & Math.pow(2, 17)) { status.deviceHeadlineDanger.text.push('High Level SD'); }
    if (reading0['141'] & Math.pow(2, 18)) { status.deviceHeadlineDanger.text.push('Low Level SD'); }
    if (reading0['141'] & Math.pow(2, 19)) { status.deviceHeadlineDanger.text.push('High Flow SD'); }
    if (reading0['141'] & Math.pow(2, 20)) { status.deviceHeadlineDanger.text.push('Low Flow SD'); }
    if (reading0['141'] & Math.pow(2, 21)) { status.deviceHeadlineDanger.text.push('High Pump Oil Temp SD'); }
    if (reading0['141'] & Math.pow(2, 22)) { status.deviceHeadlineDanger.text.push('High Pump Housing Temp SD'); }
    if (reading0['141'] & Math.pow(2, 23)) { status.deviceHeadlineDanger.text.push('Water in Fuel SD'); }
    if (reading0['141'] & Math.pow(2, 24)) { status.deviceHeadlineDanger.text.push('Low Suction SD'); }
    if (reading0['141'] & Math.pow(2, 25)) { status.deviceHeadlineDanger.text.push('High Suction SD'); }
    if (reading0['141'] & Math.pow(2, 26)) { status.deviceHeadlineDanger.text.push('High Engine Oil Pressure SD'); }
    if (reading0['141'] & Math.pow(2, 27)) { status.deviceHeadlineDanger.text.push('High Engine Oil Temp SD'); }
    if (reading0['141'] & Math.pow(2, 28)) { status.deviceHeadlineDanger.text.push('Low Gear Box Pressure SD'); }
    if (reading0['141'] & Math.pow(2, 29)) { status.deviceHeadlineDanger.text.push('High Gear Box Pressure SD'); }
    if (reading0['141'] & Math.pow(2, 30)) { status.deviceHeadlineDanger.text.push('Battery Charger Fail SD'); }
    if (reading0['141'] & Math.pow(2, 31)) { status.deviceHeadlineDanger.text.push('Red Lamp Status'); }

    status.other.push({
      id: 'windMachineControllerSDMessage',
      rank: 1,
      code: 'danger',
      category: 'controller',
      shortMessage: 'Shutdown Message from Controller',
      message: 'Refer to headline for more information on this shutdown message',
    });
  }

  // second warning
  if (reading0['142']) {
    status.deviceHeadlineWarning.enabled = true;
    status.warningIndicator = true;
    if (reading0['142'] & Math.pow(2, 0)) { status.deviceHeadlineWarning.text.push('Low Fuel Warn'); }
    if (reading0['142'] & Math.pow(2, 1)) { status.deviceHeadlineWarning.text.push('Fuel Leak Warn'); }
    if (reading0['142'] & Math.pow(2, 2)) { status.deviceHeadlineWarning.text.push('Fuel Filter Restriction Warn'); }
    if (reading0['142'] & Math.pow(2, 3)) { status.deviceHeadlineWarning.text.push('Low Lube Level W arn'); }
    if (reading0['142'] & Math.pow(2, 4)) { status.deviceHeadlineWarning.text.push('Coolant Level Warn'); }
    if (reading0['142'] & Math.pow(2, 5)) { status.deviceHeadlineWarning.text.push('Water in Fuel Warn'); }
    if (reading0['142'] & Math.pow(2, 6)) { status.deviceHeadlineWarning.text.push('No Flow Warn'); }
    if (reading0['142'] & Math.pow(2, 7)) { status.deviceHeadlineWarning.text.push('High Engine Oil Temp Warn'); }
    if (reading0['142'] & Math.pow(2, 8)) { status.deviceHeadlineWarning.text.push('Low Oil Pressure Warn'); }
    if (reading0['142'] & Math.pow(2, 9)) { status.deviceHeadlineWarning.text.push('High Engine Temp Warn'); }
    if (reading0['142'] & Math.pow(2, 10)) { status.deviceHeadlineWarning.text.push('High Discharge Pressure Warn'); }
    if (reading0['142'] & Math.pow(2, 11)) { status.deviceHeadlineWarning.text.push('Low Discharge Pressure Warn'); }
    if (reading0['142'] & Math.pow(2, 12)) { status.deviceHeadlineWarning.text.push('High Suction Warn'); }
    if (reading0['142'] & Math.pow(2, 13)) { status.deviceHeadlineWarning.text.push('Low Suction Warn'); }
    if (reading0['142'] & Math.pow(2, 14)) { status.deviceHeadlineWarning.text.push('High Level Warn'); }
    if (reading0['142'] & Math.pow(2, 15)) { status.deviceHeadlineWarning.text.push('Low Level Warn'); }
    if (reading0['142'] & Math.pow(2, 16)) { status.deviceHeadlineWarning.text.push('High Flow Warn'); }
    if (reading0['142'] & Math.pow(2, 17)) { status.deviceHeadlineWarning.text.push('Low Flow Warn'); }
    if (reading0['142'] & Math.pow(2, 18)) { status.deviceHeadlineWarning.text.push('High Pump Oil Temp Warn'); }
    if (reading0['142'] & Math.pow(2, 19)) { status.deviceHeadlineWarning.text.push('High Pump Housing Temp Warn'); }
    if (reading0['142'] & Math.pow(2, 20)) { status.deviceHeadlineWarning.text.push('Low Gear Box Pressure Warn'); }
    if (reading0['142'] & Math.pow(2, 21)) { status.deviceHeadlineWarning.text.push('High Gear Box Pressure Warn'); }
    if (reading0['142'] & Math.pow(2, 22)) { status.deviceHeadlineWarning.text.push('Air Damper Closed Warn'); }
    if (reading0['142'] & Math.pow(2, 23)) { status.deviceHeadlineWarning.text.push('Air Filter Restriction Warn'); }
    if (reading0['142'] & Math.pow(2, 24)) { status.deviceHeadlineWarning.text.push('Oil Filter Restriction Warn'); }
    if (reading0['142'] & Math.pow(2, 25)) { status.deviceHeadlineWarning.text.push('Low Engine Temp Warn'); }
    if (reading0['142'] & Math.pow(2, 26)) { status.deviceHeadlineWarning.text.push('High Engine Oil Pressure Warn'); }
    if (reading0['142'] & Math.pow(2, 27)) { status.deviceHeadlineWarning.text.push('Battery Charger Fail Warn'); }
    if (reading0['142'] & Math.pow(2, 28)) { status.deviceHeadlineWarning.text.push('Run To Destruct Warn'); }
    if (reading0['142'] & Math.pow(2, 29)) { status.deviceHeadlineWarning.text.push('Battery High Warn'); }
    if (reading0['142'] & Math.pow(2, 30)) { status.deviceHeadlineWarning.text.push('Battery Low Warn'); }
    if (reading0['142'] & Math.pow(2, 31)) { status.deviceHeadlineWarning.text.push('Amber Lamp Status'); }
    status.other.push({
      id: 'windMachineControllerWarningMessage',
      rank: 100,
      code: 'warning',
      category: 'controller',
      shortMessage: 'Warning Message from Controller',
      message: 'Refer to headline for more information on this warning message',
    });
  }
};

const controllerPv101 = function controllerPv101(status, reading0, engineRPMUp, physical) {
  const engineState = reading0[140];
  const controllerAutoMode = reading0[202];
  var controllerAutoSwitch = 0;
  if (physical.sensorAutoSwitch === true || typeof physical.sensorAutoSwitch === 'undefined') {
    controllerAutoSwitch = tools.percentToDigital(reading0[143] / 10000);
  } else if (physical.sensorAutoSwitch === false) {
    controllerAutoSwitch = 1;
  }

  if (controllerAutoMode === 1 && controllerAutoSwitch > 0) {
    status.other.push({
      id: 'windMachineControllerAutoOn',
      rank: 100,
      code: 'normal',
      category: 'controller',
      shortMessage: 'A/S Auto On',
      message: 'Wind Machine ' + physical.deviceNumber + ' controller is in auto mode',
    });
    status.stateIndicator = true;
  } else if (controllerAutoMode > 0 && controllerAutoSwitch < 1) {
    status.other.push({
      id: 'windMachineControllerSwitchOff',
      rank: 25,
      code: 'normal',
      category: 'controller',
      shortMessage: 'A/S Auto Switch Off',
      message: 'Wind Machine ' + physical.deviceNumber + ' auto mode switch is off',
    });
    status.stateIndicator = false;
    status.deviceHeadlineDanger.enabled = true;
    status.deviceHeadlineDanger.text.push('Switch on equipment is in manual position');
  } else {
    status.other.push({
      id: 'windMachineControllerAutoOff',
      rank: 25,
      code: 'normal',
      category: 'controller',
      shortMessage: 'A/S Auto Off',
      message: 'Wind Machine ' + physical.deviceNumber + ' controller is not in auto mode',
    });
    status.stateIndicator = false;
  }

  var controllerId = 'windMachine';
  var controllerRank = 10;
  var controllerCode = 'normal';
  var controllerShortMessage = 'Unknown issue';
  var controllerMessage = 'Unknown issue';

  switch (engineState) {
    case 1:
      controllerId = 'windMachineControllerEngineStopped';
      controllerRank = 100;
      controllerCode = 'normal';
      controllerShortMessage = 'Engine Stopped';
      controllerMessage = 'Wind Machine ' + physical.deviceNumber + ' is stopped';
      break;
    case 3:
      controllerId = 'windMachineControllerPrestart1';
      controllerRank = 10;
      controllerCode = 'info';
      controllerShortMessage = 'Prestart 1';
      controllerMessage = 'Wind Machine ' + physical.deviceNumber + ' in prestart delay 1';
      break;
    case 6:
      controllerId = 'windMachineControllerCrankOn';
      controllerRank = 10;
      controllerCode = 'info';
      controllerShortMessage = 'Crank on';
      controllerMessage = 'Wind Machine ' + physical.deviceNumber + ' crank on';
      break;
    case 8:
      controllerId = 'windMachineControllerFalseStart';
      controllerRank = 10;
      controllerCode = 'danger';
      controllerShortMessage = 'False Start';
      controllerMessage = 'Wind Machine ' + physical.deviceNumber + ' false start';
      break;
    case 9:
      controllerId = 'windMachineControllerWarmup';
      controllerRank = 10;
      controllerCode = 'info';
      controllerShortMessage = 'Warmup';
      controllerMessage = 'Wind Machine ' + physical.deviceNumber + ' in warmup delay';
      break;
    case 12:
      controllerId = 'windMachineControllerRunning';
      controllerRank = 10;
      controllerCode = 'success';
      controllerShortMessage = 'Running';
      controllerMessage = 'Wind Machine ' + physical.deviceNumber + ' running loaded';
      break;
    case 13:
      controllerId = 'windMachineControllerCooldown';
      controllerRank = 10;
      controllerCode = 'info';
      controllerShortMessage = 'Cooldown';
      controllerMessage = 'Wind Machine ' + physical.deviceNumber + ' in cooldown delay';
      break;
    default:
      controllerId = 'windMachineControllerUnknownEngineState';
      controllerRank = 30;
      controllerCode = 'danger';
      controllerShortMessage = 'Unknown engine state';
      controllerMessage = 'Wind Machine ' + physical.deviceNumber + ' unexpected engine state';
      break;
  }

  status.other.push({
    id: controllerId,
    rank: controllerRank,
    code: controllerCode,
    category: 'controller',
    shortMessage: controllerShortMessage,
    message: controllerMessage,
  });

  if (engineState >= 8 && engineState <= 13 && !engineRPMUp) {
    status.deviceHeadlineDanger.enabled = true;
    status.deviceHeadlineDanger.text.unshift('Machine should be ON');
    status.other.push({
      id: 'windMachineShouldBeOnState',
      rank: 1,
      code: 'danger',
      category: 'engine',
      shortMessage: 'RPM Low',
      message: 'RPM is too low for controller state at wind machine ' + physical.deviceNumber + ' ',
    });
  }
};

const controllerElectric = function controllerElectric(status, reading0, engineRPMUp, physical, tempState) {
  const engineState = reading0[140];
  const controllerAutoMode = reading0[202];
  var controllerAutoSwitch = 0;
  if (physical.sensorAutoSwitch === true || typeof physical.sensorAutoSwitch === 'undefined') {
    controllerAutoSwitch = tools.percentToDigital(reading0[143] / 10000);
  } else if (physical.sensorAutoSwitch === false) {
    controllerAutoSwitch = 1;
  }

  if (controllerAutoMode === 1 && controllerAutoSwitch > 0) {
    status.other.push({
      id: 'windMachineControllerAutoOn',
      rank: 100,
      code: 'normal',
      category: 'controller',
      shortMessage: 'A/S Auto On',
      message: 'Wind Machine ' + physical.deviceNumber + ' controller is in auto mode',
    });
    status.stateIndicator = true;
  } else if (controllerAutoMode > 0 && controllerAutoSwitch < 1) {
    status.other.push({
      id: 'windMachineControllerSwitchOff',
      rank: 25,
      code: 'normal',
      category: 'controller',
      shortMessage: 'A/S Auto Switch Off',
      message: 'Wind Machine ' + physical.deviceNumber + ' auto mode switch is off',
    });
    status.stateIndicator = false;
    status.deviceHeadlineDanger.enabled = true;
    status.deviceHeadlineDanger.text.push('Switch on equipment is in manual position');
  } else {
    status.other.push({
      id: 'windMachineControllerAutoOff',
      rank: 25,
      code: 'normal',
      category: 'controller',
      shortMessage: 'A/S Auto Off',
      message: 'Wind Machine ' + physical.deviceNumber + ' controller is not in auto mode',
    });
    status.stateIndicator = false;
  }

  var controllerId = 'windMachine';
  var controllerRank = 10;
  var controllerCode = 'normal';
  var controllerShortMessage = 'Unknown issue';
  var controllerMessage = 'Unknown issue';

  switch (engineState) {
    case 1:
      controllerId = 'windMachineControllerEngineStopped';
      controllerRank = 100;
      controllerCode = 'normal';
      controllerShortMessage = 'Engine Stopped';
      controllerMessage = 'Wind Machine ' + physical.deviceNumber + ' is stopped';
      break;
    case 6:
      controllerId = 'windMachineControllerCrankOn';
      controllerRank = 10;
      controllerCode = 'info';
      controllerShortMessage = 'Crank on';
      controllerMessage = 'Wind Machine ' + physical.deviceNumber + ' crank on';
      break;
    case 8:
      controllerId = 'windMachineControllerFalseStart';
      controllerRank = 10;
      controllerCode = 'danger';
      controllerShortMessage = 'False Start';
      controllerMessage = 'Wind Machine ' + physical.deviceNumber + ' false start';
      break;
    case 12:
      controllerId = 'windMachineControllerRunning';
      controllerRank = 10;
      controllerCode = 'success';
      controllerShortMessage = 'Running';
      controllerMessage = 'Wind Machine ' + physical.deviceNumber + ' running loaded';
      break;
    default:
      controllerId = 'windMachineControllerUnknownEngineState';
      controllerRank = 30;
      controllerCode = 'danger';
      controllerShortMessage = 'Unknown engine state';
      controllerMessage = 'Wind Machine ' + physical.deviceNumber + ' unexpected engine state';
      break;
  }

  status.other.push({
    id: controllerId,
    rank: controllerRank,
    code: controllerCode,
    category: 'controller',
    shortMessage: controllerShortMessage,
    message: controllerMessage,
  });

  if (engineState >= 8 && engineState <= 13 && !engineRPMUp) {
    status.deviceHeadlineDanger.enabled = true;
    status.deviceHeadlineDanger.text.unshift('Machine should be ON');
    status.other.push({
      id: 'windMachineShouldBeOnState',
      rank: 1,
      code: 'danger',
      category: 'engine',
      shortMessage: 'Controller Issue',
      message: 'Current switch is off, wind machine ' + physical.deviceNumber + ' should be running ',
    });
  }
};

const fuelLevelState = function fuelLevelState(status, reading0, physical) {
  var tankSize = 5.0;
  if (typeof physical === 'object' && tools.isNumber(physical.fuelTankSize)) {
    tankSize = physical.fuelTankSize;
  }
  var sensorRange = 5.557;
  if (typeof physical === 'object' && tools.isNumber(physical.fuelSensorRange)) {
    sensorRange = physical.fuelSensorRange;
  }
  var fuelLevelWarning = -1;
  if (typeof physical === 'object' && tools.isNumber(physical.fuelLevelWarning)) {
    fuelLevelWarning = physical.fuelLevelWarning / 100;
  }


  var fuelLevelConv = tools.fuelLevel(
    reading0[133] || 0,
    tankSize,
    sensorRange
  );
  

  if (fuelLevelConv >= 0 && fuelLevelConv < fuelLevelWarning) {
    status.other.push({
      id: 'windMachineFuelLow',
      rank: 50,
      code: 'normal',
      category: 'sensor',
      shortMessage: 'Fuel Low',
      message: 'Fuel level at wind machine ' + physical.deviceNumber + ' is ' + Math.round(fuelLevelConv * 100) + '% which is below the ' + Math.round(fuelLevelWarning * 100) + '% warning level.',
    });
    status.warningIndicator = true;
    status.deviceHeadlineWarning.enabled = true;
    status.deviceHeadlineWarning.text.push('Low Fuel');
  } else {
    status.other.push({
      id: 'windMachineFuelNormal',
      rank: 100,
      code: 'normal',
      category: 'sensor',
      shortMessage: 'Fuel Normal',
      message: 'Fuel level at wind machine ' + physical.deviceNumber + ' is ' + Math.round(fuelLevelConv * 100) + '%',
    });
  }
}

const powerState = function powerState(status, reading0, physical) {
  if (physical.powered) {
    const inputPower = reading0['186'] & 0x4;
    /*

        //System Status Register
        //NOTE: This is a read-only register
        REG08
        BIT
        --- VBUS status
        7: VBUS_STAT[1] | 00: Unknown (no input, or DPDM detection incomplete), 01: USB host
        6: VBUS_STAT[0] | 10: Adapter port, 11: OTG
        --- Charging status
        5: CHRG_STAT[1] | 00: Not Charging,  01: Pre-charge (<VBATLOWV)
        4: CHRG_STAT[0] | 10: Fast Charging, 11: Charge termination done
        3: DPM_STAT   0: Not DPM
                1: VINDPM or IINDPM
        2: PG_STAT    0: Power NO Good :(
                1: Power Good :)
        1: THERM_STAT 0: Normal
                1: In Thermal Regulation (HOT)
        0: VSYS_STAT  0: Not in VSYSMIN regulation (BAT > VSYSMIN)
                1: In VSYSMIN regulation (BAT < VSYSMIN)ault is 3.5V (101)
        0: Reserved

    */
    if (inputPower) {
      status.other.push({
        id: 'windMachineControllerPowerOn',
        rank: 100,
        code: 'normal',
        category: 'electricity',
        shortMessage: 'Controller has power',
        message: 'Controller has power at ' + physical.deviceNumber + ' ',
      });
    } else {
      status.other.push({
        id: 'windMachineControllerPowerOff',
        rank: 15,
        code: 'disconnected',
        category: 'controller',
        shortMessage: 'Controller does not have power',
        message: 'Controller does not have power at ' + physical.deviceNumber + ' ',
      });
      status.deviceHeadlineDanger.enabled = true;
      status.deviceHeadlineDanger.text.push('Altrac is not receiving power');
      return false;
    }
    return true;
  } else {
    const batteryExternalWarning = physical.batteryExternalWarning || 11.8;
    const batteryExternalDead = physical.batteryExternalDead || 8;
    const batteryExternal = tools.displayFormula(
      'percentTo20V',
      1,
      2,
      null,
      '130',
      reading0,
      null,
      physical
    );

    if (batteryExternal <= batteryExternalDead) {
      status.other.push({
        id: 'windMachineExternalBatteryDead',
        rank: 15,
        code: 'disconnected',
        category: 'sensor',
        shortMessage: 'Battery Dead',
        message: 'External Battery at wind machine ' + physical.deviceNumber + ' is ' + batteryExternal + 'V, engine will not start',
      });
      status.deviceHeadlineDanger.enabled = true;
      status.deviceHeadlineDanger.text.push('Dead battery');
      status.deviceHeadlineDanger.text.push('Altrac not receiving power');
      return false;
    } else if (batteryExternal <= batteryExternalWarning) {
      status.warningIndicator = true;
      status.other.push({
        id: 'windMachineExternalBatteryLow',
        rank: 80,
        code: 'normal',
        category: 'sensor',
        shortMessage: 'Battery Low',
        message: 'External Battery at wind machine ' + physical.deviceNumber + ' is ' + batteryExternal + 'V which is below the ' + batteryExternalWarning + 'V warning level.',
      });
      status.deviceHeadlineWarning.enabled = true;
      status.deviceHeadlineWarning.text.push('Low Battery');
    } else {
      status.other.push({
        id: 'windMachineExternalBatteryGood',
        rank: 100,
        code: 'normal',
        category: 'sensor',
        shortMessage: 'Battery Good',
        message: 'External Battery at wind machine ' + physical.deviceNumber + ' is ' + batteryExternal + 'V',
      });
    }
    return true;
  }
};
