// Valve.js
var tools = require('altrac-conversions');

var statusValve = function StatusValve(
  status,
  device
) {
  var reading0 = {};
  if (device.reading0) {
    reading0 = device.reading0;
  } else if (device.readingLast) {
    reading0 = device.readingLast
  }
  var valve0 = reading0['131'] & 1;
  var pressure0 = reading0['134'] & 1;
  var now = (new Date()).getTime();
  var valveTimes = tools.nextValveTimeToEpochMillis(device.application_settings.settings.valveTime);
  if (valveTimes[0] === 0 && valveTimes[1] === 0) {
    valveTimes = tools.lastValveTimeToEpochMillis(device.application_settings.settings.valveTime);
  }
  var valveStateShouldBe;
  var valveStateInfo;
  if (now < valveTimes[0] || (valveTimes[0] === 0 && valveTimes[1] === 0)) {
    valveStateShouldBe = 0;
    valveStateInfo = 'before';
  } else if (now > valveTimes[0] && now < valveTimes[1]) {
    valveStateShouldBe = 1;
    valveStateInfo = 'during';
  } else {
    valveStateShouldBe = 0;
    valveStateInfo = 'after';
  }

  var valveRank = '50';
  var valveId = 'none';
  var valveCode = 'normal';
  var valveShortMessage = 'No data';
  var valveMessage = 'No data';

  var deviceName = device && device.physical && device.physical.deviceNumber ? device.physical.deviceNumber : 'no name';

  if (valveStateShouldBe === 1) {
    if (valve0 === 1) {
      valveRank = 10;
      valveCode = 'success';
      if (pressure0 === 1) {
        valveShortMessage = 'Valve ' + deviceName + ' on';
        valveMessage = 'Valve ' + deviceName + ' is on';
        valveRank = 10;
        valveId = 'valveOnAndShouldBeOn';
        valveCode = 'success';
      } else {
        if (now - valveTimes[0] < 300000) { // 5 min
          valveShortMessage = 'Valve ' + deviceName + ' no pressure';
          valveMessage = 'Valve ' + deviceName + ' no pressure in line';
          valveRank = 2;
          valveId = 'valveOffAndShouldBeOnNoPressure';
          valveCode = 'info';
          status.deviceHeadlineWarning.enabled = true;
          status.deviceHeadlineWarning.text.push('No pressure in line');
        } else {
          valveShortMessage = 'Valve ' + deviceName + ' no pressure';
          valveMessage = 'Valve ' + deviceName + ' no pressure in line';
          valveRank = 2;
          valveId = 'valveOffAndShouldBeOnNoPressureFiveMinutes';
          valveCode = 'danger';
          status.deviceHeadlineDanger.enabled = true;
          status.deviceHeadlineDanger.text.push('No pressure in line');
        }
      }
    } else {
      valveRank = 2;
      valveCode = 'danger';
      if (pressure0 === 1) {
        valveShortMessage = 'Valve ' + deviceName + ' off, pressure on';
        valveMessage = 'Valve ' + deviceName + ' off, but pressure is on';
        valveRank = 2;
        valveId = 'valveOnAndShouldBeOff';
        valveCode = 'success';
        status.deviceHeadlineWarning.enabled = true;
        status.deviceHeadlineWarning.text.push('Solenoid off, pressure on');
      } else {
        if (now - valveTimes[0] < 300000) { // 5 min
          valveShortMessage = 'Valve ' + deviceName + ' off';
          valveMessage = 'Valve ' + deviceName + ' should be on';
          valveRank = 2;
          valveId = 'valveOffAndShouldBeOnValveNotOpen';
          valveCode = 'info';
          status.deviceHeadlineWarning.enabled = true;
          status.deviceHeadlineWarning.text.push('Valve not open');
        } else {
          valveShortMessage = 'Valve ' + deviceName + ' off';
          valveMessage = 'Valve ' + deviceName + ' should be on';
          valveRank = 2;
          valveId = 'valveOffAndShouldBeOnValveNotOpenFiveMinutes';
          valveCode = 'danger';
          status.deviceHeadlineDanger.enabled = true;
          status.deviceHeadlineDanger.text.push('Valve not open');
        } 
      }
    }
  } else {
    if (valve0 === 1) {
      valveRank = 2;
      valveCode = 'danger';
      valveShortMessage = 'Valve ' + deviceName + ' on, should be off';
      valveMessage = 'Valve ' + deviceName + ' should be off';
      valveId = 'valveOnAndShouldBeOff';
      if (pressure0 === 1) {
        if (now - valveTimes[0] < 300000) { // 5 min
          valveCode = 'info';
          status.deviceHeadlineWarning.enabled = true;
          status.deviceHeadlineWarning.text.push('Block should be off, valve open');
        } else {
          valveRank = 2;
          valveCode = 'danger';
          status.deviceHeadlineDanger.enabled = true;
          status.deviceHeadlineDanger.text.push('Block should be off, valve open');
        }
      } else {
        valveRank = 2;
        valveCode = 'normal';
        status.warningIndicator = true;
        status.deviceHeadlineWarning.enabled = true;
        status.deviceHeadlineWarning.text.push('Pressure off, valve open');
      }
    } else {
      valveRank = 50;
      valveCode = 'normal';
      if (pressure0 === 1) {
        if (valveStateInfo === 'before') {
          valveShortMessage = 'Valve ' + deviceName + ' pressure on';
          valveMessage = 'Valve ' + deviceName + ' should be off, pressure is on';
          valveRank = 2;
          valveId = 'valveOnAndShouldBeOffPressureBefore';
          valveCode = 'danger';
          status.deviceHeadlineDanger.enabled = true;
          status.deviceHeadlineDanger.text.push('Block should be off until scheduled time');
        } else {
          if (now - valveTimes[0] < 300000) { // 5 min
            valveShortMessage = 'Valve ' + deviceName + ' pressure on';
            valveMessage = 'Valve ' + deviceName + ' should be off, pressure is on';
            valveRank = 2;
            valveId = 'valveOnAndShouldBeOffPressureAfter';
            valveCode = 'info';
            status.deviceHeadlineWarning.enabled = true;
            status.deviceHeadlineWarning.text.push('Block should be off, but pressure in line');
          } else {
            valveShortMessage = 'Valve ' + deviceName + ' pressure on';
            valveMessage = 'Valve ' + deviceName + ' should be off, pressure is on';
            valveRank = 2;
            valveId = 'valveOnAndShouldBeOffPressureAfterFiveMinutes';
            valveCode = 'danger';
            status.deviceHeadlineDanger.enabled = true;
            status.deviceHeadlineDanger.text.push('Block should be off, but pressure in line');
          }
        }
      } else {
        valveShortMessage = 'Valve ' + deviceName + ' off';
        valveMessage = 'Valve ' + deviceName + ' is off';
        valveRank = 100;
        valveId = 'valveOffAndShouldBeOff';
        valveCode = 'normal';
      }
    }
  }

  status.other.push({
    rank: valveRank,
    id: valveId,
    code: valveCode,
    category: 'valve',
    shortMessage: valveShortMessage,
    message: valveMessage,
  });

  if (reading0['138'] || reading0['138'] === 0) {
    switch(reading0['138']) {
      case 2:
        status.other.push({
          rank: 1,
          id: 'solenoidDriverNotConnected',
          code: 'deviceProblem',
          category: 'sensor',
          shortMessage: 'Valve ' + deviceName + ' solenoid Driver Not Connected',
          message: 'Valve ' + deviceName + ' solenoid driver is not connected to the device',
        });
        status.deviceHeadlineDanger.enabled = true;
        status.deviceHeadlineDanger.text.push('Solenoid driver is not connected');
        break;
      case 3:
        status.other.push({
          rank: 100,
          id: 'solenoidDriverConnected',
          code: 'normal',
          category: 'sensor',
          shortMessage: 'Valve ' + deviceName + ' solenoid Driver Connected',
          message: 'Valve ' + deviceName + ' solenoid driver is connected',
        });
        break;
      default:
    }
  }

  var batteryExternalWarning = 2.2;
  var batteryExternal = tools.percentTo20V(reading0['130']);

  if (batteryExternal < batteryExternalWarning) {
    status.other.push({
      rank: 10,
      id: 'solenoidDriverLowPower',
      code: 'deviceProblem',
      category: 'sensor',
      shortMessage: 'Valve ' + deviceName + ' low SD Power',
      message: 'Valve ' + deviceName + ' solenoid power is ' + batteryExternal + 'V which is below the ' + batteryExternalWarning + 'V warning level.',
    });
    status.deviceHeadlineDanger.enabled = true;
    status.deviceHeadlineDanger.text.push('Solenoid power is below 2.3V');
  }
};

module.exports = statusValve;
