// Temperature.js
var tools = require('altrac-conversions');

var statusTemperature = function statusTemperature(
  status,
  device
) {
  const reading0 = device.reading0 || {};
  // const reading1 = device.reading1 || {};

  const setPoints = (device.physical && device.physical.temperatureSetPoints === true) ? true : false;
  const tempState128_0 = temperatureState(reading0, device.application_settings);
  
  switch(tempState128_0) {
    case 'temperatureOldReading':
    case 'temperatureBadReading':
      status.other.push({
        id: 'temperatureBadReading',
        rank: 20,
        code: 'deviceProblem',
        category: 'temperature',
        shortMessage: 'Temperature Sensor Error',
        message: 'Temperature Device ' + device.physical.deviceNumber + ' has a bad temperature reading',
      });
      status.deviceHeadlineDanger.enabled = true;
      status.deviceHeadlineDanger.text.push('Temperature probe fault');
      break;
    case 'temperatureBadSetPoints':
      status.other.push({
        id: 'temperatureBadSetPoints',
        rank: setPoints ? 20 : 100,
        code: setPoints ? 'deviceProblem' : 'normal',
        category: 'temperature',
        shortMessage: 'Bad set points',
        message: 'Temperature Device ' + device.physical.deviceNumber + ' has bad start and stop points',
      });
      break;
    case 'temperatureBelowStart':
      status.other.push({
        id: 'temperatureBelowStart',
        rank: setPoints ? 20 : 100,
        code: setPoints ? 'deviceProblem' : 'normal',
        category: 'protection',
        shortMessage: 'Temperature below start set point',
        message: 'Temperature Device ' + device.physical.deviceNumber + ' is below start set point.',
      });
      if (!setPoints) { break; }
      status.deviceHeadlineDanger.enabled = true;
      status.deviceHeadlineDanger.text.push('Ambient temperature is below start temperature');
      break;
    case 'temperatureBetweenStartAndStop':
      status.other.push({
        id: 'temperatureBetweenSetPoints',
        rank: setPoints ? 20 : 100,
        code: 'normal',
        category: 'protection',
        shortMessage: 'Temperature between set points',
        message: 'Temperature Device ' + device.physical.deviceNumber + ' is between set points.',
      });
      break;
    case 'temperatureAboveStop':
      status.other.push({
        id: 'temperatureAboveStop',
        rank: setPoints ? 50 : 100,
        code: 'normal',
        category: 'protection',
        shortMessage: 'Temperature above stop point',
        message: 'Temperature Device ' + device.physical.deviceNumber + ' is above stop point.',
      });
      break;
    default:

  }

  const inputPower = reading0['186'] & 0x4;
  if (inputPower) {
    status.other.push({
      id: 'temperatureDevicePowerOn',
      rank: 100,
      code: 'normal',
      category: 'electricity',
      shortMessage: 'Temperature Device has power',
      message: 'Temperature Device ' + device.physical.deviceNumber + ' has power.',
    });
  } else {
    status.other.push({
      id: 'temperatureDevicePowerOff',
      rank: 15,
      code: 'disconnected',
      category: 'controller',
      shortMessage: 'Temperature Device does not have power',
      message: 'Temperature Device ' + device.physical.deviceNumber + ' does not have power.',
    });
    status.deviceHeadlineDanger.enabled = true;
    status.deviceHeadlineDanger.text.push('Altrac is not receiving power');
    return false;
  }
};

module.exports = statusTemperature;

const temperatureState = function temperatureState(reading0, application_settings, temperatureKey) {
  temperatureKey = (typeof temperatureKey === 'string') ? temperatureKey : '128';
  const currentTime = (new Date()).getTime();
  const temperatureDate = new Date(reading0[temperatureKey + 'Date']).getTime();
  if (!temperatureDate || temperatureDate < (currentTime - 1000 * 60 * 60 * 2)) {
    return 'temperatureOldReading';
  }

  const temp = reading0[temperatureKey];
  var tempStart = 0;
  if (reading0 && tools.isNumber(reading0['200'])) {
    tempStart = reading0['200'];
  } else if (application_settings && application_settings.settings.tempStart) {
    tempStart = application_settings.settings.tempStart;
  }
  var tempStop = 0;
  if (reading0 && tools.isNumber(reading0['201'])) {
    tempStop = reading0['201'];
  } else if (application_settings && application_settings.settings.tempStop) {
    tempStop = application_settings.settings.tempStop;
  }

  if (
    temp &&
    (
      temp === 327.67 ||
      temp <= -40 ||
      temp >= 85
    )
  ) {
    return 'temperatureBadReading';
  } else if (tempStart > tempStop) {
    return 'temperatureBadSetPoints';
  } else if (temp < tempStart) {
    return 'temperatureBelowStart';
  } else if (temp >= tempStart && temp <= tempStop) {
    return 'temperatureBetweenStartAndStop';
  } else if (temp > tempStop) {
    return 'temperatureAboveStop';
  } else {
    return 'temperatureBadReading';
  }
}

module.exports.temperatureState = temperatureState;
