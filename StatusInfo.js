// StatusInfo.js
var _sortBy = require('lodash/sortBy');

var statusWindMachine = require('./WindMachine');
var statusPump = require('./Pump');
var statusValve = require('./Valve');
var statusTemperature = require('./Temperature');
var statusWeather = require('./Weather');

var statusCalculate = function statusCalculate(device, callback) {
  // code options:
  // normal
  // primary
  // success
  // info
  // warning
  // danger
  // see bottom for examples of return values

  var reading0 = {};
  if (device.reading0) {
    reading0 = device.reading0;
  } else if (device.readingLast) {
    reading0 = device.readingLast
  }

  var status = {
    code: 'normal',
    shortMessage: 'OK',
    message: 'No Issues',
    id: 'none',
    other: [],
    deviceHeadlineDanger: {
      enabled: false,
      text: [],
      color: 'danger'
    },
    deviceHeadlineWarning: {
      enabled: false,
      text: [],
      color: 'warning'
    },
    warningIndicator: false,
    stateIndicator: false
  };

  var deviceType = (device.physical && device.physical.deviceType) ? device.physical.deviceType : 'Unknown';
  if (
    device.interface_versioned &&
    device.interface_versioned.data &&
    device.interface_versioned.data.other &&
    typeof device.interface_versioned.data.other.deviceType === 'string'
  ) {
    deviceType = device.interface_versioned.data.other.deviceType;
  }

  /*


    variables required for status calculating


  */

  var connected = true;
  var currentTime = (new Date()).getTime();
  var sleepInterval = (reading0['135'] * 1000);
  var resetTimeAllowed = 6 * 60 * 1000;
  if (reading0['135'] === 0 || typeof reading0['135'] === 'undefined') {
    sleepInterval = 10 * 60 * 1000; // up to 10 min between checkins when sleep is 0
    resetTimeAllowed = 3 * 60 * 1000;
  }

  // 30 sec is length of time it might take a device to connect cloud after waking
  var nextSeen = new Date(reading0.date).getTime() + sleepInterval + (30 * 1000);
  const missedCheckinMilliseconds = currentTime - nextSeen;
  var minutesPast = Math.round((missedCheckinMilliseconds) / 1000 / 60);

  /*


    altrac device connection

    We want the device to show offline gradually to give the customer the best
    information possible

    This varies with what the device is doing. If its online all the time, 
    it should show as disconnected quicker. If its online only every 15
    min, it should show offline after a longer period of time

    not sleeping resets should take 3 minute
    sleeping resets should take 6 minutes
      bc reconnecting to tower takes longer
      and they're not as critical if the device is sleeping

    if its online all the time or asleep less than or equal to 3 minutes:
      it should checkin every 10 minutes
        if we don't see it for 19 minutes (1 missed check plus 3 resets)
          show yellow indicator its offline 
            after 31 minutes show red indicator its offline (1 missed, 7 resets)
              after 43 minutes show offline red wifi icon (1 miseed, 11 resets)
    if its sleeping for more than 3 minutes
      it should checkin to match the last sleepInterval (e.g. 15 min)
        after 33 minutes show yellow indicator its offline (1 missed, 3 sleep resets)
          after 57 minutes show red indicator its offline (1 missed, 7 sleep resets)
            after 81 minutes show offline red wifi icon (1 miseed, 11 sleep resets)


  */

  if (missedCheckinMilliseconds > resetTimeAllowed * 11) {
    connected = false;
    status.other.push({
      id: 'notConnected',
      rank: 1,
      code: 'deviceProblem',
      category: 'device',
      shortMessage: 'Not Connected',
      message: 'Device ' + device.physical.deviceNumber + ' is ' + minutesPast + ' minutes late reporting',
    });
    status.deviceHeadlineDanger.enabled = true;
    status.deviceHeadlineDanger.text.push('Altrac device is not connected to the cloud.');
  } else if (missedCheckinMilliseconds > resetTimeAllowed * 7) {
    status.other.push({
      id: 'deviceMissedCheckinDanger',
      rank: 5,
      code: 'danger',
      category: 'device',
      shortMessage: 'Not Connected',
      message: 'Device ' + device.physical.deviceNumber + ' is ' + minutesPast + ' minutes late reporting',
    });
    status.deviceHeadlineDanger.enabled = true;
    status.deviceHeadlineDanger.text.push('Altrac device has not checked in with cloud for ' + minutesPast + ' minutes.');
  } else if (missedCheckinMilliseconds > resetTimeAllowed * 3) {
    status.other.push({
      id: 'deviceMissedCheckinWarning',
      rank: 100,
      code: 'normal',
      category: 'device',
      shortMessage: 'Not Connected',
      message: 'Device ' + device.physical.deviceNumber + ' is ' + minutesPast + ' minutes late reporting',
    });
    status.warningIndicator = true;
    status.deviceHeadlineWarning.enabled = true;
    status.deviceHeadlineWarning.text.push('Altrac device has not checked in with cloud for ' + minutesPast + ' minutes.');
  } else {
    status.other.push({
      id: 'connected',
      rank: 100,
      code: 'normal',
      category: 'device',
      shortMessage: 'Connected',
      message: 'Device ' + device.physical.deviceNumber + ' is connected',
    });
  }
  

  if (connected) {
    if (deviceType === 'windMachine') { statusWindMachine(status, device); }
    if (deviceType === 'coldAirDrain') { statusWindMachine(status, device); }
    if (deviceType === 'pumpFrostWater') { statusWindMachine(status, device); }
    if (deviceType === 'pump') { statusPump(status, device); }
    if (deviceType === 'flow') { statusPump(status, device); }
    if (deviceType === 'valve') { statusValve(status, device); }
    if (deviceType === 'temperature') { statusTemperature(status, device); }
    if (deviceType === 'weather') { statusWeather(status, device); }
  }

  /*


    sensor


  */

  var batteryInternalWarning = 0.20;
  var batteryInternal = reading0['129'];

  if (batteryInternal < batteryInternalWarning) {
    status.other.push({
      id: 'batteryLow',
      rank: 100,
      code: 'normal',
      category: 'sensor',
      shortMessage: 'Altrac Battery Low',
      message: 'Int Batt at ' + device.physical.deviceNumber + ' is ' + Math.round(batteryInternal * 100) + '% which is below the ' + Math.round(batteryInternalWarning * 100) + '% warning level.',
    });
    if (connected) {
      status.warningIndicator = true;
      status.deviceHeadlineWarning.enabled = true;
      status.deviceHeadlineWarning.text.push('Altrac internal battery low');
    }
  }

  status.other = _sortBy(status.other, [function (o) { return o.rank; }]);

  if (
    status.other.length > 0
  ) {
    status.code = status.other[0].code;
    status.id = status.other[0].id;
    status.category = 'multiple';
    status.shortMessage = status.other[0].shortMessage;
    status.message = status.other[0].message;
  } else {
    status.code = status.other[0].code;
    status.id = status.other[0].id;
    status.category = status.other[0].category;
    status.shortMessage = status.other[0].shortMessage;
    status.message = status.other[0].message;
  }

  if (callback) { callback(null, status); }
  return status;
};

module.exports = statusCalculate;

/* this should return something like this:

var status = {
  code: 'warning',
  message: 'Multiple issues',
  other: [
    {
      rank: 10,
      code: 'warning',
      category: 'controller',
      message: 'Engine controller is not Connected',
    }, {
      rank: 10,
      code: 'warning',
      category: 'controller',
      message: 'Engine controller is not in autostart mode',
    }, {
      rank: 5,
      code: 'warning',
      category: 'device',
      message: 'Internal battery is less than 20%',
    },
  ],
};

var status = {
  code: 'danger',
  shortMessage: 'Engine fault',
  message: 'Engine did not start properly',
  other: [
    {
      rank: 1,
      code: 'danger',
      category: 'engine',
      shortMessage: 'Engine fault',
      message: 'Engine did not start properly',
    }, {
      rank: 10,
      code: 'warning',
      category: 'controller',
      shortMessage: 'Autostart Off',
      message: 'Engine controller is not in autostart mode',
    }, {
      rank: 5,
      code: 'warning',
      category: 'device',
      shortMessage: 'Low device batt',
      message: 'Internal battery is less than 20%',
    },
  ],
};

*/
